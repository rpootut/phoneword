﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Phoneword
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        string translatedNumber;
        public MainPage()
        {
            InitializeComponent();
        }
        private void OnTranslate(object sender, EventArgs e)
        {
            string enteredNumber = txtPhoneNumber.Text;
            translatedNumber = PhonewordTranslator.ToNumber(enteredNumber);

            if (string.IsNullOrEmpty(translatedNumber))
            {
                btnCall.IsEnabled = false;
                btnCall.Text = "Llamar";
            }
            else
            {
                btnCall.IsEnabled = true;
                btnCall.Text = "Llamar: " + translatedNumber;
            }
        }

        async private void OnCall(object sender, EventArgs e)
        {
            if (await DisplayAlert(
                "Marcar un numero",
                "¿Te gustaria marcar a: " + translatedNumber + "?",
                "Si",
                "No"
                ))
            {
                try
                {
                    PhoneDialer.Open(translatedNumber);
                }
                catch (Exception)
                {
                    await DisplayAlert("No se puede marcar", "El marcado del numero fallo", "OK");
                }
            }
        }
    }
}