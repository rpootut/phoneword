﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Phoneword
{
    class MainPageOld : ContentPage
    {
        Entry txtPhoneNumber;
        Button btnTranslate;
        Button btnCall;
        string translatedNumber;

        public MainPageOld()
        {
            Padding = new Thickness(20, 20, 20, 20);

            StackLayout panel = new StackLayout
            {
                Spacing = 15
            };

            panel.Children.Add(new Label
            {
                Text = "Introduce una palabra telefonica",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
            });

            panel.Children.Add(txtPhoneNumber = new Entry
            {
                Text = "1-800-IFE2000"
            });

            panel.Children.Add(btnTranslate = new Button
            {
                Text = "Traducir"
            });

            panel.Children.Add(btnCall = new Button
            {
                Text = "Llamar",
                IsEnabled = false
            });


            btnTranslate.Clicked += OnTranslate;
            btnCall.Clicked += OnCall;
            Content = panel;
        }

        private void OnTranslate(object sender, EventArgs e)
        {
            string enteredNumber = txtPhoneNumber.Text;
            translatedNumber = PhonewordTranslator.ToNumber(enteredNumber);

            if (string.IsNullOrEmpty(translatedNumber))
            {
                btnCall.IsEnabled = false;
                btnCall.Text = "Llamar";
            }
            else
            {
                btnCall.IsEnabled = true;
                btnCall.Text = "Llamar: " + translatedNumber;
            }
        }

        async private void OnCall(object sender, EventArgs e)
        {
            if (await DisplayAlert(
                "Marcar un numero",
                "¿Te gustaria marcar a: " + translatedNumber + "?",
                "Si",
                "No"
                ))
            {
                try
                {
                    PhoneDialer.Open(translatedNumber);
                }
                catch (Exception)
                {
                    await DisplayAlert("No se puede marcar", "El marcado del numero fallo", "OK");
                }
            }
        }
    }
}
